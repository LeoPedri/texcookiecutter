# texCookieCutter

## Description

Simple python script to initialize an empty LaTeX project.

## Installation

Clone the directory, `cd` into it and run

```bash
$ pip install .
```

to install the cookie cutter.

## Usage

To run the script execute

```bash
$ create_tex
```

and then just follow the provided prompts.
