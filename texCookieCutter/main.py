from pathlib import Path
import subprocess

SIGNATURE = '%%% This file was generated with a cookie cutter template %%%\n'


def main():
    path = input('Path to initialize the project in (defaults to cwd) >> ')
    title = input('Title of the project >> ')
    subtitle = input('Subtitle of the project >> ')
    submission_date = input('Submission date >> ')
    experiment_date = input('Experiment date >> ')
    supervisor = input('Supervisor name and title >> ')

    if not path:
        path = Path.cwd()
    else:
        path = Path(path)
        path.mkdir(parents=True, exist_ok=True)

    # initialize directories
    Path(path / 'src').mkdir(parents=True, exist_ok=True)
    Path(path / 'img').mkdir(parents=True, exist_ok=True)
    subprocess.run(['cp', (Path(__file__).parent.parent / 'assets/uibk_logo.jpg'), (path / 'img')])

    # initialize template files
    with open(path / 'macros.def', mode='a') as macros:
        macros.write(SIGNATURE)

    with open(path / 'literature.bib', mode='a') as literature:
        literature.write(SIGNATURE)

    with open(path / 'packages.sty', mode='a') as packages:
        packages.write(SIGNATURE +
r'''
\ProvidesPackage{packages}

\usepackage[margin=1in]{geometry}
\usepackage[strict]{changepage}
\usepackage[english]{babel}
\usepackage{float}
\usepackage{fancyhdr}
\usepackage{booktabs}
\usepackage{mhchem}
\usepackage{siunitx}
\usepackage{wrapfig, booktabs}
\usepackage{adjustbox}
\usepackage{enumitem}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{commath}
\usepackage{xurl}
\usepackage{hyperref}
\usepackage{amsmath,mathtools}
\usepackage{tikz}
\usepackage[hang]{footmisc}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{chemformula}
\usepackage{amsfonts}
\usepackage{diagbox}
\usepackage{pdfpages}
\usepackage{tabu}
\usepackage{tabularx} 
\usepackage{mathrsfs}
\usepackage{color}
\usepackage{sectsty}
''')

    with open(path / 'settings.sty', mode='a') as settings:
        settings.write(SIGNATURE +
r'''
\linespread{1.25}
\pagestyle{fancy}
\fancyhf{}
\rhead{\instituteName}
% \lhead{YOUR HEADER GOES HERE}
\rfoot{page \thepage}
\setlength{\parindent}{0pt}
\bibliographystyle{ieeetr}
\hypersetup{colorlinks=true,linkcolor=blue}
''')

    with open(path / 'coverpage.tex', mode='a') as coverpage:
        subtitle = subtitle if subtitle else 'YOUR SUBTITLE GOES HERE'
        title = title if title else 'YOUR TITLE GOES HERE'
        submission_date = submission_date if submission_date else r'\today'
        experiment_date = experiment_date if experiment_date else 'DATE OF THE EXPERIMENT GORS HERE'
        supervisor = supervisor if supervisor else 'NAME GOES HERE'
        coverpage.write(SIGNATURE +
r'\title{\Large ' + subtitle + r'\\[0.5cm]' + '\n' +
r'\bf\huge ' + title + '}' +
r'''
\author{\large Leonardo Pedri (11820439) \\ \ \\}
\date{\large Submission date: ''' + submission_date + '}'+ '\n' +
r'\newcommand{\experimentDate}{' + experiment_date + '}' +
r'\newcommand{\Supervisor}{Supervisor: ' + supervisor + '}' +
r'''

\makeatletter
    \begin{titlepage}
        \begin{center}
	   { \includegraphics[width=12cm]{img/uibk_logo.jpg}}
	   {\ \\ \ \\}
        \vbox{}\vspace{5cm}
            {\@title}\\[3cm] 
            {\@author}
            {\large \Supervisor  \\ \ \\}
            {\large \experimentDate  \\ \ \\}
            {\@date\\}

        \end{center}
    \end{titlepage}
\makeatother
''')

    with open(path / 'main.tex', mode='a') as entry:
        entry.write(SIGNATURE +
r'''
\documentclass[11pt,titlepage,bibliography=totoc]{scrartcl}

\usepackage{packages}
\input{settings.sty}
\input{macros.def}



% Preamble
\begin{document}
\input{coverpage}
\tableofcontents
\clearpage

% WRITE YOUR SECTIONS IN src AND INSERT THEM IN THE MAIN DOCUMENT LIKE SO
% \input{src/...}
% \clearpage

% Appendix
\bibliography{literature}

\clearpage
\listoffigures
\listoftables 

\end{document}
''')


if __name__ == '__main__':
    main()